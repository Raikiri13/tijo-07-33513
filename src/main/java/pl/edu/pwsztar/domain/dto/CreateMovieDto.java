package pl.edu.pwsztar.domain.dto;

import java.io.Serializable;

public class CreateMovieDto implements Serializable {
    private String title;
    private String image;
    private Integer year;
    private String videoId;

    public CreateMovieDto() {
    }

    public CreateMovieDto(CreateMovieDtoBuilder builder){
        this.title = builder.title;
        this.image = builder.image;
        this.year = builder.year;
        this.videoId = builder.videoId;
    }

    public String getTitle() {
        return title;
    }

    public String getImage() {
        return image;
    }

    public Integer getYear() {
        return year;
    }

    public String getVideoId() {
        return videoId;
    }

    @Override
    public String toString() {
        return "CreateMovieDto{" +
                "title='" + title + '\'' +
                ", image='" + image + '\'' +
                ", year=" + year +
                ", videoId='" + videoId + '\'' +
                '}';
    }

    public static final class CreateMovieDtoBuilder {
        private String title;
        private String image;
        private Integer year;
        private String videoId;

        public CreateMovieDtoBuilder(){
        }

        public CreateMovieDtoBuilder title(String title){
            this.title = title;
            return this;
        }

        public CreateMovieDtoBuilder image(String image){
            this.image = image;
            return this;
        }

        public CreateMovieDtoBuilder year(Integer year){
            this.year = year;
            return this;
        }

        public CreateMovieDtoBuilder videoId(String videoId){
            this.videoId = videoId;
            return this;
        }

        public CreateMovieDto build(){
            return new CreateMovieDto(this);
        }
    }

}
