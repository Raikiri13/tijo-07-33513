package pl.edu.pwsztar.domain.dto;

public class DetailsMovieDto {
    private String title;
    private String videoId;
    private String image;
    private Integer year;

    public DetailsMovieDto(){}

    public DetailsMovieDto(DetailsMovieDtoBuilder builder) {
        this.title = builder.title;
        this.image = builder.image;
        this.year = builder.year;
        this.videoId = builder.videoId;
    }

    public String getTitle() {
        return title;
    }

    public String getVideoId() {
        return videoId;
    }

    public String getImage() {
        return image;
    }

    public Integer getYear() {
        return year;
    }

    @Override
    public String toString() {
        return "DetailsMovieDto{" +
                "title='" + title + '\'' +
                ", videoId='" + videoId + '\'' +
                '}';
    }

    public static final class DetailsMovieDtoBuilder {
        private String title;
        private String image;
        private Integer year;
        private String videoId;

        public DetailsMovieDtoBuilder(){
        }

        public DetailsMovieDtoBuilder title(String title){
            this.title = title;
            return this;
        }

        public DetailsMovieDtoBuilder image(String image){
            this.image = image;
            return this;
        }

        public DetailsMovieDtoBuilder year(Integer year){
            this.year = year;
            return this;
        }

        public DetailsMovieDtoBuilder videoId(String videoId){
            this.videoId = videoId;
            return this;
        }

        public DetailsMovieDto build(){
            return new DetailsMovieDto(this);
        }
    }

}
