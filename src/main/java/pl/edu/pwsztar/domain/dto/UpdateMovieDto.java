package pl.edu.pwsztar.domain.dto;

import java.io.Serializable;

public class UpdateMovieDto implements Serializable {
    private String title;
    private String image;
    private Integer year;
    private String videoId;

    public UpdateMovieDto() {
    }

    public UpdateMovieDto(UpdateMovieDtoBuilder builder) {
        this.title = builder.title;
        this.image = builder.image;
        this.year = builder.year;
        this.videoId = builder.videoId;
    }

    public String getTitle() {
        return title;
    }

    public String getImage() {
        return image;
    }

    public Integer getYear() {
        return year;
    }

    public String getVideoId() {
        return videoId;
    }

    @Override
    public String toString() {
        return "CreateMovieDto{" +
                "title='" + title + '\'' +
                ", image='" + image + '\'' +
                ", year=" + year +
                ", videoId='" + videoId + '\'' +
                '}';
    }

    public static final class UpdateMovieDtoBuilder {
        private String title;
        private String image;
        private Integer year;
        private String videoId;

        public UpdateMovieDtoBuilder(){
        }

        public UpdateMovieDtoBuilder title(String title){
            this.title = title;
            return this;
        }

        public UpdateMovieDtoBuilder image(String image){
            this.image = image;
            return this;
        }

        public UpdateMovieDtoBuilder year(Integer year){
            this.year = year;
            return this;
        }

        public UpdateMovieDtoBuilder videoId(String videoId){
            this.videoId = videoId;
            return this;
        }

        public UpdateMovieDto build(){
            return new UpdateMovieDto(this);
        }
    }

}
