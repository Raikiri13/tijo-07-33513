package pl.edu.pwsztar.domain.dto;

import java.io.Serializable;

public class MovieDto implements Serializable {

    private Long movieId;
    private String title;
    private String image;
    private Integer year;

    public MovieDto() {
    }

    public MovieDto(MovieDtoBuilder builder) {
        this.title = builder.title;
        this.image = builder.image;
        this.year = builder.year;
        this.movieId = builder.movieId;
    }

    public Long getMovieId() {
        return movieId;
    }

    public String getTitle() {
        return title;
    }

    public String getImage() {
        return image;
    }

    public Integer getYear() {
        return year;
    }

    public static final class MovieDtoBuilder {
        private String title;
        private String image;
        private Integer year;
        private Long movieId;

        public MovieDtoBuilder(){
        }

        public MovieDtoBuilder title(String title){
            this.title = title;
            return this;
        }

        public MovieDtoBuilder image(String image){
            this.image = image;
            return this;
        }

        public MovieDtoBuilder year(Integer year){
            this.year = year;
            return this;
        }

        public MovieDtoBuilder movieId(Long movieId){
            this.movieId = movieId;
            return this;
        }

        public MovieDto build(){
            return new MovieDto(this);
        }
    }

}
