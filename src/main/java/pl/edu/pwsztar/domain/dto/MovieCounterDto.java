package pl.edu.pwsztar.domain.dto;

public class MovieCounterDto {

    private long counter;

    public MovieCounterDto() {
    }

    public MovieCounterDto(MovieCounterDtoBuilder builder) {
        this.counter = builder.counter;
    }

    public long getCounter() {
        return counter;
    }

    @Override
    public String toString() {
        return "MovieCounterDto{" +
                "counter=" + counter +
                '}';
    }

    public static final class MovieCounterDtoBuilder {
        private long counter;

        public MovieCounterDtoBuilder(){
        }

        public MovieCounterDtoBuilder counter(long counter){
            this.counter = counter;
            return this;
        }

        public MovieCounterDto build(){
            return new MovieCounterDto(this);
        }
    }
}
