package pl.edu.pwsztar.domain.entity;

import pl.edu.pwsztar.domain.dto.DetailsMovieDto;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "movies")
public class Movie implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "movie_id")
    private Long movieId;

    @Column(name = "title")
    private String title;

    @Column(name = "image")
    private String image;

    @Column(name = "year")
    private Integer year;

    @Column(name = "video_id")
    private String videoId;

    public Movie() {
    }

    public Movie(MovieBuilder builder){
        this.title = builder.title;
        this.image = builder.image;
        this.year = builder.year;
        this.videoId = builder.videoId;
        this.movieId = builder.movieId;
    }

    public void setMovieId(Long movieId) {
        this.movieId = movieId;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    public Long getMovieId() {
        return movieId;
    }

    public String getTitle() {
        return title;
    }

    public String getImage() {
        return image;
    }

    public Integer getYear() {
        return year;
    }

    public String getVideoId() {
        return videoId;
    }

    public static final class MovieBuilder {
        private String title;
        private String image;
        private Integer year;
        private String videoId;
        private Long movieId;

        public MovieBuilder(){
        }

        public MovieBuilder title(String title){
            this.title = title;
            return this;
        }

        public MovieBuilder image(String image){
            this.image = image;
            return this;
        }

        public MovieBuilder year(Integer year){
            this.year = year;
            return this;
        }

        public MovieBuilder videoId(String videoId){
            this.videoId = videoId;
            return this;
        }

        public MovieBuilder movieId(Long movieId){
            this.movieId = movieId;
            return this;
        }

        public Movie build(){
            return new Movie(this);
        }
    }

}
